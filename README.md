# DevOps Infrastructure

This directory contains Vagrantfile and all required files to create VMs in Openstack and VirtualBox.

# Running vagrant

## Prerequisites

### OpenStack

* You need an OpenStack project.
* vagrant-openstack-provider plugin.
* VirtualBox 6.0+

## Run vagrant for Openstack

To create instances in Openstack update the **environment.bat** file with required values:

1. Update **environment.bat**:

```BAT
set OS_AUTH_URL=
set OS_PROJECT_NAME=
set OS_DOMAIN_NAME=
set OS_USERNAME=
set OS_PASSWORD=
set OS_KEY_PAIR_NAME=
set OS_PRIVATE_KEY_PATH=
set OS_INITIALS=
```

2. Execute the next commands:

```
c:\>environment.bat
c:\>vagrant up server-1
c:\>vagrant up server-2
```

## Run vagrant for VirtualBox

To create an VMs in VirtualBox execute fo to **vagrant_virtualbox** directory and execute the next commands:

```
c:\>vagrant up vb-server-1
c:\>vagrant up vb-server-2
```
